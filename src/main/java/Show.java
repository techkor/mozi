
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hornak.bence
 */
public class Show {
    Movie movie;
    Date begin;
    Ticket[][] tickets;
    Room room;
    
    Show(Movie movie2, Date begin, Room room) {
        tickets = new Ticket[room.rows][room.cols];
        movie = movie2;
        this.begin = begin;
        this.room = room;
    }
}
