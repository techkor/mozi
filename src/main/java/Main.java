
import com.google.zxing.WriterException;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;
import org.simplejavamail.mailer.config.TransportStrategy;

public class Main {

    public static void main(String[] args) throws WriterException, IOException {
        Movie creed2 = new Movie();
        creed2.title = "Creed II";
        creed2.duration = 130;
        creed2.minAge = 12;
        creed2.releaseYear = 2018;

        Room room2 = new Room();
        room2.num = 2;
        room2.cols = 10;
        room2.rows = 30;

        Show show1 = new Show(creed2, new Date(2019, 01, 20, 20, 00), room2);

        Cinema myCinema = new Cinema();
        myCinema.shows = new Show[]{show1};

        /*myCinema.buyTicket(show1, "hornak.bence@gmail.com",
                "Hornák Bence", "student", 0, 1);
        myCinema.buyTicket(show1, "hornak.bence@gmail.com",
                "Hornák Bence", "student", 0, 1);*/
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                for (int i = 0; i < myCinema.shows.length; i++) {
                    Show show = myCinema.shows[i];
                    System.out.println(i + ".: " + show.movie.title);
                }
                int sorszam = sc.nextInt();
                sc.nextLine();
                System.out.println("Add meg az e-mail címedet:");
                String email = sc.nextLine();
                System.out.println("Add meg a nevedet:");
                String name = sc.nextLine();
                System.out.println("Add meg a jegy típusát:");
                String type = sc.nextLine();
                System.out.println("Add meg a helyet (sor, oszlop):");
                int hely_sor = sc.nextInt();
                int hely_oszlop = sc.nextInt();

                if (sorszam < 0 || sorszam >= myCinema.shows.length) {
                    continue;
                }

                myCinema.buyTicket(myCinema.shows[sorszam], email,
                        name, type, hely_sor, hely_oszlop);
            } catch(Exception e) {
                System.out.println("Hiba");
            }
        }
    }
}
