
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;
import org.simplejavamail.mailer.config.TransportStrategy;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hornak.bence
 */
public class Cinema {
    Show[] shows;
    
    public void buyTicket(Show show, String email, String name,
            String type, int row, int col) throws WriterException, IOException {
        
        if(show.tickets[row][col] != null) {
            System.out.println("Foglalt a hely!");
            return;
        }
        
        Ticket ticket = new Ticket();
        ticket.email = email;
        ticket.name = name;
        ticket.type = type;
        show.tickets[row][col] = ticket;
        
        sendEmail(ticket, show);
    }
    
    public void sendEmail(Ticket ticket, Show show) throws WriterException, IOException{
        System.out.println("Sending email");
        
        String text = "A vásárlásod sikeres volt, várunk szerettel.\n"
                + "Dátum: "+show.begin+"\n"
                + "Film: "+show.movie.title;
        
        Email email = EmailBuilder.startingBlank()
                .to(ticket.name, ticket.email)
                .from("Mr. mozi", "jegy@techkor.hu")
                .withSubject("Köszönjük a vásárlást!")
                .withPlainText(text)
                .withEmbeddedImage("ticket", getQRCodeImage("Email: "+ticket.email, 512, 512), "image/png")
                .buildEmail();

        Mailer mailer = MailerBuilder
                .withSMTPServer("mail.techkor.hu", 465, "jegy@techkor.hu", "tEx#IsbPux.Q")
                .withTransportStrategy(TransportStrategy.SMTPS)
                .buildMailer();

        mailer.sendMail(email);
        System.out.println("Email sent");
    }
    private static byte[] getQRCodeImage(String text, int width, int height) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
        byte[] pngData = pngOutputStream.toByteArray(); 
        return pngData;
    }
}
